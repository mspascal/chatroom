import json

import os
import requests

from asgiref.sync import async_to_sync
from celery import Celery, shared_task

# set the default Django settings module for the 'celery' program.
#from chat import consumers
from channels.layers import get_channel_layer
from django.conf import settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'chatroom.settings')

app = Celery('chatroom')
app.conf.broker_url = 'redis://localhost:6379/0'
# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@shared_task
def process_quote_task(room_name, stock_quote):

    url = settings.STOCK_QUOTE_API_URL.format(stock_quote)
    try:
        r = requests.get(url, timeout=10)
        data = r.content.decode().split("\n")[1]
        stock_name = data.split(',')[0]
        quote = data.split(',')[6]
        if quote == 'N/D':
            message = f"Quote is not defined for {stock_name}."
        else:
            message = f"{stock_name} quote is ${quote} per share."
    except (IndexError, requests.exceptions.ConnectionError) as e:
        message = "Quote could not be retrieved due to an internal error."


    channel_layer = get_channel_layer()
    # Send message to room group
    async_to_sync(channel_layer.group_send)(
        room_name,
        {
            'type': 'chat_message',
            'message': f"[Bot]: {message}"
        }
    )