# Financial chat

This project implements a simple browser-based chat application using Django and channels.

It supports the /stock command to get a stock quote. For example, it is possible run the command as follows:
```
   /stock=aapl.us 
```
This command is encharge of run (in Background, via celery) a task that call an external API and get the "Close" value from the second row of the CVS downloaded.

It is supported multiple chatrooms.


## Deployment
After this repo was cloned, some steps are required:

0. Create a new virtual environment using the Python version 3.8.6 and your favorite virtualenv management utility. The following examples use the venv module shipped with Python 3:

    For Linux use the following command.
    ``` bash
    python3 -m venv venv
    ```
    For Windows:

    If you dont have virtualenv installed, type the following:
    ``` bash
    python get-pip.py
    pip install virtualenv
    ```
    If python3 could not be found, install python3 and add the path of the python3 interpreter to the system path 

    enter the cp-backend directory and type:
    ``` bash
    virtualvenv venv
    ```

0. Activate and install the projects requirements into the virtual environment:

    For Linux, type the following:
    ```bash
    source venv/bin/activate
    pip install -r requirements.txt
    ```

    For Windows, do this:
    ```bash
    venv\Scripts\activate.bat
    ```

    Now install the requirements:
    ```bash
    pip install -r requirements.txt

0. Run Migrations
    ```
    ./manage.py migrate
    ```

0. The channel layer uses Redis as its backing store. To start a Redis server on port 6379, run the following command:
    ```
    docker run -p 6379:6379 -d redis:5
    ```

0. Start the celery worker:
    ```
    celery --app=chatroom worker 
    ```

0. Start the server:
    ```
    ./manage.py runserver 
    ```

0. Open a browser tab at http://127.0.0.1:8000/chat/ and enter a room name. Open a second browser tab and enter to the same room name.

0. You should be able to chat and run the command "/stock"

## Testing
To run the automated testing some requirements are needed:

0. Chrome web browser
0. Chromedriver https://sites.google.com/a/chromium.org/chromedriver/getting-started

0. Once the requirements are satisfied, it is possible to run the tests:
    ```
        ./manage.py test chat.tests
    ```
