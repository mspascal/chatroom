# chat/views.py
from django.shortcuts import render

from chat.models import Message


def index(request):
    return render(request, 'chat/index.html')

def room(request, room_name, user_name):
    return render(request, 'chat/room.html', {
        'room_name': room_name,
        'user_name': user_name,
        'messages': Message.get_last_messages(room_name)
    })