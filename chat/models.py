from django.db import models

# Create your models here.
from django.conf import settings


class Message(models.Model):
    user_name = models.CharField(
        max_length=20,
        help_text='The name for the user.'
    )

    room_name = models.CharField(
        max_length=100,
        help_text='The name for the room.'
    )

    message = models.CharField(
        max_length=100,
        help_text='The message sent.'
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    @staticmethod
    def get_last_messages(room_name:str):
        messages = Message.objects.filter(room_name=room_name).order_by('-created_at')[:settings.HISTORY_ROOM_MESSAGES_LIMIT]

        data = [f"[{m.user_name}]: {m.message}" for m in reversed(messages)]

        return data